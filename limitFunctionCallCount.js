function cb() {
    // body...
    console.log("callback invoked");
}

function limitFunctionCallCount(cb, n) {
    // body...
    let counter = n;
    function invoke() {
        // body...
        if(counter-- > 0){
            cb();
        }
        else{
            console.log(`Callback already called ${n} times`);
        }
    }
    return {invoke};
}

let nTimes = limitFunctionCallCount(cb, 3);
nTimes.invoke();
nTimes.invoke();
nTimes.invoke();
nTimes.invoke();
