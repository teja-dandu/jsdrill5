
function cacheFunction(cb) {
    const cache = {};

    if(!cb) return null;
    const newFunction = (input) => {
        if(Object.prototype.hasOwnProperty.call(cache, input)){
            return cache[input];
        }
        cache[input] = cb(input);
        return cache[input];
    };
    return newFunction;
}


module.exports = {
    cacheFunction,
};

